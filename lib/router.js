Router.configure({
	layoutTemplate: "layout",
	loadingTemplate: 'loading',
	waitOn: function() { 
		// return Meteor.subscribe("alladdresses", 20) && 
		//        Meteor.subscribe("csv");
		return  Meteor.subscribe("csv");
	}
});
Router.route("/", function () {
	Session.set("popover", null);
	this.render("addressTable");
});

Router.route("/show/:_id", function() {
	var id = this.params._id;
	Session.set("fullInfo", {"name": "Загружаю данные..."});
	Meteor.call("show", id, function(err, result) {
		Session.set("fullInfo", result);
	});
	Session.set("popover", "show");
	this.render("addressTable");
});

Router.route("/about", function () {
	Session.set("popover", "about");
	this.render("addressTable");
});

Router.onBeforeAction('loading');