var ITEMS_INCREMENT = 20;

Template.addressTable.onRendered(function() {
  // Defaults
  Session.setDefault("searchInAddressesChecked", true);
  // Set autorun function
  this.autorun(function() {
    if (Session.equals("popover", "show")) {
      $("#show-modal").modal('show');
    } else if (Session.equals("popover", "about")) {
      $("#about-modal").modal('show');
    } else {
      $(".modal").modal("hide");
    }
  });

  // Events
  $(".modal").on('hide.bs.modal', function() {
    Router.go("/");
  });

  // Popovers
  if (Session.equals("popover", "show")) {
    GAnalytics.pageview("/posledniy_adres_show");
    // var hintElem = $(".address-row")[0].children[2];
    // $(hintElem).popover({
    //   "animation": true,
    //   "delay": { "show": 0, "hide": 3000 },
    //   "content": "Нажмите, чтобы увидеть полную справку.",
    //   "container": hintElem,
    //   "placement": "right",
    //   "trigger": "hover"
    // });
    // $(hintElem).mouseenter().mouseleave();
    // $(hintElem).on('hidden.bs.popover', function () {
    //   $(hintElem).popover("destroy");
    // }); 
  } else if (Session.equals("popover", "about")) {
    GAnalytics.pageview("/posledniy_adres_about");
  } else {
    GAnalytics.pageview("/posledniy_adres_root");
  }
});

Template.addressTable.helpers({
  "rows": function() {
    return Collections.Addresses.find();
  },
  "moreResults": function() {
    // If, once the subscription is ready, we have less rows than we
    // asked for, we've got all the rows in the collection.
    var count =  Collections.Addresses.find().count();
    Session.set("count", count);
    return !(count < Session.get("itemsLimit"));
  },
  "fullInfo": function() {
    return Session.get("fullInfo");
  }
});

Template.addressTable.events({
  "click .address-row": function(event) {
    GAnalytics.event("posledniy_adres", "show");
    Router.go("/show/" + event.currentTarget.getAttribute("id"));
  },
});

Template.search.helpers({
  "searchInAddressesChecked": function() {
    if (Session.equals("searchInAddressesChecked", true)) {
      return {"checked": true};
    }
  },
  "searchString": function() {
    return Session.get("searchString");
  },
  "houseNumber": function() {
    return Session.get("houseNumber");
  },
  "searchInAddress": function() {
    return Session.get("searchInAddressesChecked");
  },
  "count": function() {
    return Session.get("count");
  }
});
var strokeTimer;
Template.search.events({
  "click #searchInAddress": function(event) {
    event.preventDefault();
    Session.set('itemsLimit', ITEMS_INCREMENT);
    Session.set("searchInAddressesChecked", event.currentTarget.checked);
  },
  "input #searchString, propertychange #searchString": function(event, template) {
    clearTimeout(strokeTimer);
    strokeTimer = setTimeout(function() {
      Session.set('itemsLimit', ITEMS_INCREMENT);
      var searchString = template.$("#searchString").val();
      Session.set("searchString", searchString);
    }, 500);
  },
  "input #houseNumber, propertychange #houseNumber": function(event, template) {
    clearTimeout(strokeTimer);
    strokeTimer = setTimeout(function() {
      Session.set('itemsLimit', ITEMS_INCREMENT);
      var houseNumber = template.$("#houseNumber").val();
      Session.set("houseNumber", houseNumber);
    }, 500);
  },
  "submit form": function(event) {
    event.preventDefault();
  }
});

//==============================================================================
// Infinit scroll
//==============================================================================
Session.setDefault('itemsLimit', ITEMS_INCREMENT);

Tracker.autorun(function() {
  $(".form-control-feedback").addClass("glyphicon glyphicon-search");
  var start = Date.now();
  Meteor.subscribe("alladdresses",
                   Session.get('itemsLimit'),
                   Session.get('searchInAddressesChecked'),
                   Session.get('searchString'),
                   Session.get("houseNumber"),
                   function() {
                    console.log("query duration",  Date.now() - start);
                    $(".form-control-feedback").removeClass("glyphicon glyphicon-search");
                   });
});

// whenever #showMoreResults becomes visible, retrieve more results
function showMoreVisible() {
    var threshold, target = $("#showMoreResults");
    if (!target.length) return;
 
    threshold = $(window).scrollTop() + $(window).height() - target.height();
    if (target.offset().top <= threshold) {
        if (!target.data("visible")) {
            target.data("visible", true);
            Session.set("itemsLimit",
                Session.get("itemsLimit") + ITEMS_INCREMENT);
        }
    } else {
        if (target.data("visible")) {
            target.data("visible", false);
        }
    }        
}

$(window).scroll(showMoreVisible);