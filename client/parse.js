var getCurrentFile = function() {
  var file = Collections.Csv.findOne({uploadedAt: {$exists: false}});
  if (file === undefined) {
    file = Collections.Csv.findOne();
  }
  return file;
}
Template.parse.helpers({
  "message": function () {
    var file = getCurrentFile();
    if (file === undefined) {
      if (Collections.Addresses.find().count() === 0) {
        return "База адресов пуста. Пожалуйста, загрузите данные.";
      } else {
        return "Информация о файле базы неизвестна.";
      }
    }
    if (file.isUploaded()) {
      return "Текущая база была загружена " +
             file.uploadedAt.toLocaleString("ru-RU") 
             + " из файла \"" +
             file.original.name + "\" с последним временем изменения " + 
             file.original.updatedAt.toLocaleString("ru-RU") + ".";
     } else {
      return "Пожалуйста, подождите, файл загружается.";
     }
  },
  "csvFile": function() {
    return getCurrentFile();
  },
  "show": function() {
    var file = getCurrentFile();
    if (file === undefined || !file.isUploaded()) {
      return true;
    } else {
      return false;
    }
  }
});
Template.parse.events({
  "change #tsv-to-parse": function(event, template) {
    event.preventDefault();
    var file = template.find("#tsv-to-parse").files[0];
    if (file === undefined) {
      return;
    }
    Collections.Csv.insert(file, function(err, fileObj) {
      if (err !== undefined) {
        console.error(err);
      }
    });
  }
});