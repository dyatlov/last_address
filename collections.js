//==============================================================================
// StatQuerties
//==============================================================================

Collections.StatQueries = new Meteor.Collection("stat_queries");

//==============================================================================
// Adrresses
//==============================================================================

Collections.AddressesFields = [
  "sameAddress",
  "recordNumber",
  "name",
  "gender",
  "yob",
  "localityType",
  "localityName",
  "originalTextReference",
  "placeOfBirth",
  "streetName",
  "modernStreetName",
  "houseNumber",
  "apartmentNumber",
  "fullAddress",
  "nationality",
  "profession",
  "note",
  "partyMember",
  "placeOfJob",
  "arrestDate",
  "sentence",
  "code",
  "placeOfExecution"
];

Collections.Addresses = new Meteor.Collection("addresses");

if (Meteor.isServer) {
  // Indexes
  Collections.Addresses._ensureIndex("name");
  var fieldsToShow =  [ "name",
                        "yob",
                        "fullAddress",
                        "modernStreetName",
                        "profession",
                        "arrestDate",
                        "code"];
  var projection = {};
  _.each(fieldsToShow, function(field) {
    projection[field] = 1;
  });

  // Returns {"street": ["невский", "проспект", ...], "number": [1, 102, 5, ...]}
  var parseAddressSearchString = function(searchString, houseNumber) {
    var res = {street:[], number:[]};

    res.street = searchString.trim().toLowerCase();
    res.street = res.street.match(/([а-яА-Я]+)|([0-9]+)/g);
    res.street = _.uniq(res.street);

    if (houseNumber !== undefined && houseNumber !== null) {
      res.number = houseNumber.trim();
      res.number = res.number.match(/([0-9]+)/g);
      res.number = _.uniq(res.number);
    }

    return res;
  };
  // Returns Mongo query for address search
  var composeAddressQuery = function(queryParams) {
    // console.log(queryParams);
    var reStreetNameStr = "";
    _.each(queryParams.street, function(word, index) {
      if (isNaN(parseInt(word))) {
        reStreetNameStr += "(?=.*" + word + ".*)";
      } else {
        reStreetNameStr += "(([^\\d]+|^)" + word + "([^\\d]+|$))";
      }
    });
    var reHouseNumberStr = ""
    _.each(queryParams.number, function(word, index) {
      reHouseNumberStr += "(([^\\d]+|^)" + word + "([^\\d]+|$))|";
    });
    reHouseNumberStr = reHouseNumberStr.substring(0, reHouseNumberStr.length - 1);
    var reStreetName = new RegExp(reStreetNameStr, "i");
    var reHouseNumber = new RegExp(reHouseNumberStr);
    var query = {};
    if (queryParams.number.length > 0) {
      query = {"houseNumber": reHouseNumber, 
               "$or": [{"streetName"      : reStreetName},
                       {"modernStreetName": reStreetName}]};
    } else {
      query = {"$or": [{"streetName"      : reStreetName},
                       {"modernStreetName": reStreetName}]};
    }
    return query;
  };

  // Returns Mongo query for full search
  var composeFullQuery = function(words) {
    var reFullStr = "";
    _.each(words, function(word, index) {
      reFullStr += "(?=.*" + word + ".*)";
    });
    var reFull = new RegExp(reFullStr, "i");
    query = {"$or": [{"originalTextReference"      : reFull},
                     {"modernStreetName"           : reFull}]};
    return query;
  };

  var logSearchString = function(searchInAddress,
                                 searchString,
                                 houseNumber)
  {
    if (searchString !== null && searchString.trim().length !== 0) {
      var strToLog = searchString;
      if (searchInAddress) {
        strToLog += " #" + houseNumber;
      }
      var statQuery = {
        "timestamp": Date.now(),
        "searchInAddress": searchInAddress,
        "searchString": searchString
      };
      if (searchInAddress) {
        statQuery.houseNumber = houseNumber;
      }
      console.log(strToLog);
      Collections.StatQueries.insert(statQuery);
    } else {
      console.log("Show all");
    }
  };

  Meteor.publish("alladdresses", function(limit, searchInAddress, searchString, houseNumber) {
    if (searchInAddress === null) {
      searchInAddress = true;
    }
    var query = {};
    if (searchString !== null && searchString.trim().length > 2) 
    {
      if (searchInAddress) {
        var queryParams = parseAddressSearchString(searchString, houseNumber);
        if (queryParams.street.length !== 0) {
          query = composeAddressQuery(queryParams);
        }
      } else {
        var words = searchString.trim().toLowerCase();
        words = words.match(/([а-яА-Я]+)|([0-9]+)/g);
        words = _.uniq(words);
        if (words.length !== 0) {
          query = composeFullQuery(words);
        }
      }
    }
    logSearchString(searchInAddress, searchString, houseNumber);
    return res = Collections.Addresses.find(query,
                                            {"limit": limit, 
                                             "sort": {"name": 1},
                                             "fields": projection});
  });
}

//==============================================================================
// CSV file
//==============================================================================

var csvStore = new FS.Store.GridFS("csv");
Collections.Csv = new FS.Collection("csv", {"stores": [csvStore]});
if (Meteor.isServer) {
  Meteor.publish("csv", function() {
    return Collections.Csv.find();
  });

  var parseFile = function(readStream) {
    var readline = Npm.require("readline");
    var rl = readline.createInterface({
      input: readStream,
      terminal: false
    });
    var firstLinePassed = false;
    var boundCallback = Meteor.bindEnvironment(function(line) {
      if (!firstLinePassed) {
        firstLinePassed = true;
        return;
      }
      var values = Baby.parse(line).data[0];
      var document = {};
      _.each(values, function(value, index) {
        document[Collections.AddressesFields[index]] = value;
      });
      Collections.Addresses.insert(document);
    });
    rl.on("line", boundCallback);
    return true;
   }

  Collections.Csv.find({uploadedAt: {$exists: false}}).observe({
    "added": function(file) {
      var queryHandle = Collections.Csv.find(file._id).observeChanges({
        "changed": function() {
          if (file.isUploaded()) {
            queryHandle.stop();
            // Remove old DB and file
            Collections.Addresses.remove({});
            Collections.Csv.remove({_id: {$ne: file._id}});
            var readStream = file.createReadStream(); 
            parseFile(readStream);
          }
        }
      });
    }
  })
}
